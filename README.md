Welcome at the Wellnesshotel Mittelburg Allgäu! Our family-run 4-star Superior hotel is appealingly located on a little hill and offers you a free view of wide meadows and the Allgäu mountains.

Address: Mittelburgweg 1, 87466 Oy-Mittelberg, Bayern, Germany

Phone: +49 8366 180
